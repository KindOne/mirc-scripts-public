Collection of various scripts.


Scripts that require spaces.dll:
  ascii.mrc
  autocorrect.mrc


The spaces.dll can be found here:
  http://www.xise.nl/mirc/


Version specific scripts:
  autocorrect.mrc
    7.33 and newer.
  away.mrc
    7.61 and newer.
  znc-server-time-fix.mrc
  	7.33 - 7.41

Contact:

I can be found one various IRC networks as the nick "KindOne".


  Libera.Chat
  EFnet
  OFTC
  IRCnet
  DALnet