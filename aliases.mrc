; Alias tab.
ame { echo -ag [/ame] ... }
amsg { echo -ag [/amsg] ... }

calc { echo -ag *** $1- = $calc( $1- ) }

cat { .msg *shell cat .znc/users/ $+ $me $+ /networks/ $+ $network $+ /moddata/log/ $+ $lower($active) $+ _ $+ $asctime(yyyymmdd) $+ .log }

; Copy entire Channel/query/status buffer..
; Created by: Wiz126
; Date: Dec 25, 2012
; Original:
; //var %l = 1, %s = $line(#, 0) | clipboard | while (%l <= %s) { clipboard -an $line(#, %l) | inc %l }
chancopy {
  var %l = 1, %s = $line($active, 0)
  clipboard
  while (%l <= %s) {
    clipboard -an $line($active, %l)
    inc %l
  }
}



cite {
  if ($1) { say $1 $+ , [[citation needed]] }
  else { say [[citation needed]] }
}


cycle hop

; Typo protection.
dex dec

disapproval {
  if ($1) { say $1 $+ : ಠ_ಠ  }
  else { say ಠ_ಠ  }
}

; Evaluation - Used to calculate how longs it takes. disapproval
; Example: /eval if ($nick == KindOne) { msg $chan cool story bro } | else { halt }
eval var %x = $ticks | say (Eval): [[ $+ $1-] == [[ $+ $($1-,2) $+ ]] [[ $+ $calc($ticks -%x) $+ ms]

google { say http://lmgtfy.com/?q= $+ $replace($$1-, $chr(32), +) }

hex { echo -ag $longip($base($1, 16, 10)) }


logdir { run $logdir $+ $network }

; mass cycle
; Use this instead of '%x $chan(0)' and 'dec %x' cause that would include
; open channels that have been kick/banned from ..
masscycle {
  var %x 1
  while ($comchan($me,%x)) {
    hop $v1
    inc %x
  }
}




; List loaded scripts.
; Syntax: /modlist
modlist {
  if (o !isin $usermode) {
    var %x $script(0)
    var %x 1
    var %y $sha1($script(%x),2)
    while (%x <= $script(0)) {
      echo -ag $script(%x) $asctime($file($script(%x)).mtime)
      inc %x
    }
  }
  else { raw modlist }
}


; Load a file.
; Syntax: /modload words
; * Loaded script 'C:\mIRC\scripts\words.mrc'
modload {
  if (o !isin $usermode) {
    if ($1) {
      if (*.mrc !iswm $1) {
        if ($isfile($scriptdir $+ $1 $+ .mrc)) { load -rs " $+ $scriptdir $+ $1 $+ .mrc" }
        else { echo -ag [/modload] No such file. }
      }
      else { load -rs " $+ $scriptdir $+ $1 $+ " }
    }
    else { echo -ag [/modload] Include a module you fucking idiot. }
  }
  else { quote modload $1 }
}


; Unload a script....
; /modunload lolwut
; * Unloaded script 'C:\mIRC\scripts\lolwut.mrc'
modunload {
  if (o !isin $usermode) {
    if ($1) {
      var %x = $iif($script($1.ini), $v1, $script($1.mrc))
      if (%x) {
        .unload -rs $qt(%x)
        echo -cga info * %x was unloaded
      }
      else { echo -cga info * No script by the name $qt($1) was found. }
    }
    else { echo -ag [/modunload] Include a module you fucking idiot. }
  }
  else { quote modunload $1 }
}



; Created by jaytea
; This turns sent notices into red (4).
; This overrides's mIRC's defaults.
notice .!notice $1- | echo 4 -eat -> $+(-, $1, -) $2-


nuke { clearall -a }


reverse { say $regsubex($1-, /(.)/gs, $mid(\A, -\n, 1)) }

; DO NOT ALTER THE SPACING BELOW.
safe return $!decode( $encode( $1- ,m) ,m)
; DO NOT ALTER THE SPACING ABOVE.

slap me slaps $1 around a bit with a large trout

test { say test failed. (a)bort, (i)gnore, (r)etry, (f)ail }

; Shortcut alias for /whowas
; Syntax: /waa KindOne
waa {
  if ($1) { whowas $1 $1 }
  else { echo -ag [/waa] You forgot the nick. }
}

; Shortcut alias for /whois
; Syntax: /wii KindOne
wii {
  if ($1) { whois $1 $1 }
  else { echo -ag [/wii] You forgot the nick. }
}

; raw.mrc script and others use this.
wd { return $gettok($1,$2 $+ $3,32) }


/*
* This Alias will show you if you have anything sitting in your edit box of channels.
* Syntax: /where
* Created by "Raccoon"
* June 30, 2012.
* alias name altered by me...
*/
where scon -a what
what {
  var %i = 1, %n = $window(*,0)
  while (%i <= %n) {
    if $editbox($window(*,%i)) {
      echo -agic info * $network : $window(*,%i) - $v1
    }
    inc %i
  }
}


; Copy/paste entire "slash and asterisk" comments from various programming languages into channels
;     and query windows without the need of CTRL+ENTER.
; <@KindOne> /*
; <@KindOne> * foobar
; <@KindOne> */
; <@KindOne> /* 4 lines of actual code and 11 lines of comments...  */
; Wiz126 thinks this is some sort of quirk. Works in alias tab. Works in remote tab with "alias".
; "Discovered" by myself on June 18 2012. (Note to self: check SwiftIRC/#msl for logs on this).
; TODO: Figure out how to handle "// comment goes here", if thats even possible. 99.999% sure its not.
* {
  ; Line with just "/*" will fail without this.
  if (!$1) { say /* }
  ; Line with the "/* foobar ...".
  else { say /* $1- }
}