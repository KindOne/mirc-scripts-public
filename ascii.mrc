; $chr(0) - $chr(31) give all sorts of printout issues, eating spaces, and stuff.
; Since only useful things are colour/bold/italics/reverse/underline just skip them.

; Needs Sat's spaces.dll

alias ascii {
  var %x 0
  var %y 32
  var %z 64
  var %a 96
  echo -ag  +------------------+------------------+-------------------+
  while (%x < 32) {
    $dll(spaces.dll, echo, -ag $&
      $chr(124) $str($chr(32),3) %y $str($chr(32),3) $chr(%y) $str($chr(32),3) $&
      $chr(124) $str($chr(32),3) %z $str($chr(32),3) $chr(%z) $str($chr(32),3) $&
      $chr(124) $iif($left(%a,1) = 9,$str($chr(32),4),$str($chr(32),3)) $&
      %a $str($chr(32),3) $chr(%a) $str($chr(32),3) $chr(124))
    inc %x
    inc %y
    inc %z
    inc %a
  }
  echo -ag  +------------------+------------------+-------------------+
  $dll(spaces.dll, echo, -ag Controls:  ^B 2  $chr(124) ^C 3 $chr(124) ^R 22  $chr(124) ^I 29 $chr(124) ^U 31)
}
