; mIRC autobleh - Updated: Jan 1, 2016
; Created by KindOne - irc.freenode.net ##kindone
; This script is based on the irssi version (Credits for them. Awesome script)  - http://autobleh.projectnet.org/

; Note: this copy does not have the timers
; The following commands work in the syntax

; TODO:
;   Clean this up. I dont like how it looks.

; Syntax:

; /ab KindOne       - Ban
; /ak KindTwo       - Kick
; /aq MeanOne       - Quiet on hostmask/cloak - (charybdis)
; /ar EvilOne       - Remove - (charybdis)
; /abk lolwut       - Kick/Ban on hostmask/cloak
; /abr ChanServ     - Remove/Ban on hostmask/cloak

alias ab {
  if ($me isop $chan) { mode $chan +b $address($1,2) }
  else { set %command_1 mode $chan +b-o $address($1,2) $me | get_op
  }
}

alias ak {
  if ($me isop $chan) { kick $chan $1 you should know better }
  else {
    set %command_1 kick $chan $1 you should know better
    set %command_2 mode $chan -o $me
    get_op
  }
}

alias aq {
  if ($me isop $chan) { mode $chan +q $address($1,2) }
  else { set %command_1 mode $chan +q-o $address($1,2) $me | get_op }
}

alias abr {
  if ($me isop $chan) {
    mode $chan +b $address($1,2) $+ $chr(36) $+ ##arguments
    .raw remove $chan $1 :you should know better.
  }
  else {
    set %command_1  mode $chan +b $address($1,2) $+ $chr(36) $+ ##arguments
    set %command_2  .raw remove $chan $1 :you should know better.
    set %command_3  mode $chan -o $me
    get_op
  }
}

alias abk {
  if ($me isop $chan) {
    mode $chan +b $address($1,2)
    kick $chan $1 you should know better
  }
  else {
    set %command_1 mode $chan +b $address($1,2)
    set %command_2 kick $chan $1 you should know better
    set %command_3 mode $chan -o $me
    get_op
  }
}

alias ar {
  if ($me isop $chan) { .raw remove $chan $1 :you should know better. }
  else { set %command_2 .raw remove $chan $1 :you should know better. | get_op }
}


on *:op:#:{
  if (($opnick == $me) && (%command_1)) {
    %command_1
    %command_2
    %command_3
    unset %command_1
    unset %command_2
    unset %command_3
  }
}

alias -l get_op {
  .msg ChanServ op $chan $me
}

; Freenode specific.
; "A Bad Connection"
alias abc {
  if ($me isop $chan) { mode $chan +b $address($1,2) $+ $ $+ ##fix_your_connection }
  else { set %command_1 mode $chan +b-o $address($1,2) $+ $ $+ ##fix_your_connection $me | get_op }
}

; EOF
