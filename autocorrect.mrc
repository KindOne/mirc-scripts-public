; autocorrect.mrc
;
; Script was originally called 'ProjectX Spellchecker' created by 'ProjectX - mIRC Scripting Crew'.
; Almost none of the original code exists.

; Requirements:
; mIRC 7.33 or newer. The script uses "var -n". Without "var -n" typing "1 - 10" would print "-9".
; Saturn's spaces.dll which can be found at http://www.xise.nl/mirc/

; Credit goes to Saturn for writing the $regsubex() code and the spaces.dll.

; TODO:
; - Combine the /msg, /me, and /notice into one? If possible?
; - Keep upper case? Currently " DONT -> don't " .. needs to be " DONT -> DON'T "
; - Deal with people not having the spaces.dll downloaded.
; - Add - - around the nick in the /notice.

; I use C:\mIRC\scripts\tables\autocorrect.dat
alias -l autocorrect { return $+(",$scriptdirtables\autocorrect.dat,") }

on *:input:*:{

  if ($left($1,1) != /) && (!$ctrlenter) {

    var -n %temp $dll(spaces.dll, input, )
    var -n %temp = $regsubex(%temp,/((?<=^| )\w+)/g,$iif($hget(autocorrect,\1) != $null,$v1,\1))

    $dll(spaces.dll, send, PRIVMSG $target : $+ %temp)
    $dll(spaces.dll, echo, -aitc own $+(<,$me,>) %temp)
    halt
  }

  ; /me

  if ($left($1,3) == /me) && ($1 != /menubar) && (!$ctrlenter) {

    var -n %temp $dll(spaces.dll, input, )
    var -n %temp = $regsubex(%temp,/((?<=^| )\w+)/g,$iif($hget(autocorrect,\1) != $null,$v1,\1))

    $dll(spaces.dll, send, PRIVMSG $target :ACTION $right(%temp,-4) $+ )
    $dll(spaces.dll, echo, -cat action * $me $right(%temp,-4))
    halt
  }

  ; /notice

  if ($left($1,7) == /notice) && (!$ctrlenter) {

    var -n %temp $dll(spaces.dll, input, )
    var -n %temp = $regsubex(%temp,/((?<=^| )\w+)/g,$iif($hget(autocorrect,\1) != $null,$v1,\1))

    $dll(spaces.dll, send, NOTICE : $+ $right(%temp,-8))
    $dll(spaces.dll, echo, 4 -eat -> $right(%temp,-8))
    halt
  }
}


on *:start:{
  ; Script uses "var -n". Only in 7.33 and newer.
  if ($version < 7.33) { echo -a mIRC 7.33 or newer needed! Unloading autocorrect.mrc | .unload -rs $+(",$script,") }
  if (!$hget(autocorrect)) { hmake autocorrect 2000 }
  if ($isfile($autocorrect)) { hload autocorrect $autocorrect }
}

on *:unload: {
  if ($hget(autocorrect)) {
    hsave autocorrect $autocorrect
    hfree autocorrect
  }
}

on *:exit: {
  if ($hget(autocorrect)) {
    hsave autocorrect $autocorrect
  }
}