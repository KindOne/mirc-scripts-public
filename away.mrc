; For mIRC 7.61 or newer if you want instant updates.
; 01/03/2020 - mIRC v7.61
; ...
; 5.Fixed /cnick not updating nick color immediately when used with a
;  full address.
; ...
; 15/04/2017 - mIRC v7.48
; ...
; 18.Added CAP support for away-notify...
; https://ircv3.net/specs/extensions/away-notify-3.1

; NOTES:
; Currently set to use grey ( ctrl+k 14 ) because Xchat/HexChat uses that.
; Any nicks the address book ( /abook -l ) with this colour will be deleted
; when this script is loaded / unloaded or client connects / disconnects.

; If you don't like that colour, you can change it to something else below.
; Anything using the colour you pick will be deleted.

; mIRC does not have multi-network support for "Nick Colors".
; This script attempts to fix some of those issue by using hash tables.

; KNOWN ISSUES:
; If someone is on multiple networks with the same nick!ident@host
; without (cloaking/spoofs/vhosts/whatever) and they only change the
; /away status on one network the status change will be applied for
; all other networks.

; ZNC: If you are in a large amount of channels you might get output in the
; status window due to this:
; https://forums.mirc.com/ubbthreads.php/topics/260197//ialfill_WHOX_995_output_visib

; TODO: Delete people that are /away if I part a channel with only them.

on *:start:{
  .cnick on

  ; Change this for a different colour.
  set %away-nick-colour 14

  away-nick-delete
  if ($hget(away-nick-colour)) { hfree away-nick-colour }
  hmake away-nick-colour 200
}

on *:unload:{
  away-nick-delete
  if ($hget(away-nick-colour)) { hfree away-nick-colour }
  unset %away-nick-colour
}

; Delete all entries with the %away-nick-color when closing mIRC.
on *:exit: { away-nick-delete }

on *:connect:{
  if ($hget(away-nick-colour)) {
    ; Connecting or reconnecting on a network, delete all entries only for that network if any exists.
    ; Use the $right($calc($len(...))) to remove "$network_" from the "nick!ident@host" hash table item.
    noop $hfind(away-nick-colour,$network $+ _*,0,w,.cnick -r $right($1,- $+ $calc( $len($network) + 1))))
    noop $hfind(away-nick-colour,$network $+ _*,0,w,hdel away-nick-colour $1)
  }
}

on *:disconnect:{
  if ($hget(away-nick-colour)) {
    ; Disconnected on a network, delete all entries only for that network if any exists.
    ; Use the $right($calc($len(...))) to remove "$network_" from the "nick!ident@host" hash table item.
    noop $hfind(away-nick-colour,$network $+ _*,0,w,.cnick -r $right($1,- $+ $calc( $len($network) + 1))))
    noop $hfind(away-nick-colour,$network $+ _*,0,w,hdel away-nick-colour $1)
  }
}

; User set or unset /away, change the nick.
raw away:*:{
  if ($1 != $null) {
    .cnick $fulladdress %away-nick-colour
    hadd away-nick-colour $+($network,_,$fulladdress)
  }
  if ($1 == $null) {
    .cnick -r $fulladdress
    hdel away-nick-colour $+($network,_,$fulladdress)
  }
}

; Unset /away
raw 305:*:{
  .cnick -r $address($me,5)
  hdel away-nick-colour $+($network,_,$address($me,5))
}
; /away ...
raw 306:*:{
  .cnick $address($me,5) %away-nick-colour
  hadd away-nick-colour $+($network,_,$address($me,5))
}

; /who #channel || /ialfill #channel (no /whox support)
raw 352:*: {
  if (G isin $7) {
    .cnick $+($6,!,$3,@,$4) %away-nick-colour
    hadd away-nick-colour $+($network,_,$6,!,$3,@,$4)
  }
}

; /ialfill #channel (/whox support)
raw 354:*:{
  if (($2 == 995) && (G isin $8)) {
    .cnick $+($7,!,$4,@,$5) %away-nick-colour
    hadd away-nick-colour $+($network,_,$7,!,$4,@,$5)
  }
}

on *:join:#:{
  if ($nick == $me) {
    .ialfill $chan
  }
}

on *:nick:{
  ; Delete old nick add new one if they are /away.
  if ($cnick($fulladdress)) {
    .cnick -r $fulladdress
    .cnick $address($newnick,5) %away-nick-colour
    hdel away-nick-colour $+($network,_,$fulladdress)
    hadd away-nick-colour $+($network,_,$address($newnick,5))
  }
}
on *:quit:{
  .cnick -r $fulladdress
  hdel away-nick-colour $+($network,_,$fulladdress)
}

on *:part:#:{
  ; I parted all channels. Delete all users for that network.
  if ($nick == $me) && ($chan(0) == 1) {
    ; Use the $right($calc($len(...))) to remove "$network_" from the "nick!ident@host" hash table item.
    noop $hfind(away-nick-colour,$network $+ _*,0,w,.cnick -r $right($1,- $+ $calc( $len($network) + 1))))
    noop $hfind(away-nick-colour,$network $+ _*,0,w,hdel away-nick-colour $1)
  }
  ; Delete the nick if we do not share any channels if they /part one.
  if ($comchan($nick,0) == 1) {
    .cnick -r $fulladdress
    hdel away-nick-colour $+($network,_,$fulladdress)
  }
}

; Only delete the nicks ( /abook -l ) that are using the colour the script is using.
alias -l away-nick-delete {
  var %x $cnick(0)
  while (%x) {
    if ($cnick(%x).colour == %away-nick-colour) {
      .cnick -r $cnick(%x)
    }
    dec %x
  }
}
