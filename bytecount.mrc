menu * {
  Byte Counter: dialog -m bytes bytes
}

dialog Bytes {
  title "Byte Counter"
  size -1 -1 228 179
  option dbu
  edit "", 1, 12 9 199 136, multi return hsbar vsbar
  edit "0", 2, 24 161 29 10, read autohs center
  edit "0", 3, 85 161 29 10, read autohs center
  text "Bytes", 4, 5 161 17 8
  text "KiloBytes", 5, 59 161 25 8
  box "Text", 6, 2 1 220 150
  box "Size", 7, 2 152 116 24
  button "Exit", 8, 201 161 23 10, cancel
  button "Clear", 9, 174 161 23 10
  box "Lines", 10, 123 152 45 24
  edit "", 11, 128 161 33 10, read autovs center
}

on *:DIALOG:bytes:init:0: { .timerbyte -m 0 100 did -ra bytes 11 $!did(bytes,1,1).sel $!+ / $!+ $!did(bytes,1).lines }
on *:DIALOG:bytes:close:0: { .timerbyte off }
on *:DIALOG:bytes:edit:1: {
  var %x = 1, %len
  while (%x <= $did(bytes,1).lines) {
    var %i = $regsubex($did(bytes,1,%x),/^\s*;.*|^\s*|\s*$/g,)
    if ($left(%i,2) == /*) {
      var %comment on
    }
    if ($right(%i,2) == */ && %comment == on) {
      var %comment off
    }
    elseif (%comment != on) {
      inc %len $len(%i)
    }
    inc %x
  }
  did -ra bytes 2 %len
  did -ra bytes 3 $bytes(%len,k)
}
on *:DIALOG:bytes:sclick:9: { did -ra bytes 2,3 0 | did -r bytes 1 }
