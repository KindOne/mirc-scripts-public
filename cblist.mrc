; Use: /cblist
alias cblist {
  ; Open the window, and clear it just in case it was already opened
  window @myWindow
  clear @myWindow

  ; Set the %i, our counting variable, to the start value of 1, and
  ; set the %x variable to the amount of lines in the clipboard
  var %i = 1, %x = $cb(0)

  ; Loop until %x has gone through all clipboard contents
  while (%i <= %x) {
    echo @myWindow $cb(%i)
    inc %i
  }
  echo @myWindow Clipboard contents finished!
}
