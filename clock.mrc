alias clock {
  if ($1 = update) {
    var %hrs $asctime(hh)
    var %min $asctime(nn)
    var %hrsw $replacex($iif(%min > 30,$calc(%hrs + 1),%hrs),11,ELEVEN,12,TWELVE,10,TEN,1,ONE,2,TWO,3,THREE,4,FOUR,5,FIVE,6,SIX,7,SEVEN,8,EIGHT,9,NINE)
    var %minw $replacex(%min,11,ELEVEN,12,TWELVE,10,TEN,1,ONE,2,TWO,3,THREE,4,FOUR,5,FIVE,6,SIX,7,SEVEN,8,EIGHT,9,NINE)
    tokenize 32 $clc(IT IS) HALF TEN. QUARTER TWENTY. FIVE MINUTES TO. PAST ONE THREE. TWO FOUR FIVE. SIX SEVEN EIGHT. NINE TEN ELEVEN. TWELVE
    if (%min > 58) || (%min < 2) var %end $1-10 $replace($11-,%hrsw,$clc(%hrsw)) $clc(O'CLOCK)
    elseif (%min < 7) var %end $1-6 $clc($7) $8-9 $clc($10) $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 12) var %end $1-3 $clc($4) $+ $5-9 $clc($10) $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 17) var %end $1-4 $clc($5) $6-9 $clc($10) $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 25) var %end $1-5 $clc($6) $7-9 $clc($10) $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 37) var %end $1-2 $clc($3) $4-9 $clc($10) $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 42) var %end $1-5 $clc($6) %7 $clc($8-9) $10 $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 47) var %end $1-4 $clc($5) $6-8 $clc($9) $10 $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 52) var %end $1-3 $clc($4) $5-7 $clc($8-9) $10 $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    elseif (%min < 58) var %end $1-6 $clc($7-9) $10 $replace($11-,%hrsw,$clc(%hrsw)) O'CLOCK
    tokenize 46 %end
    clear @clock
    aline 14 @clock $*
    .timer 1 60 clock update
  }
  else {
    window -kbBDao +bf @clock -1 -1 250 250 Verdana 22
    .timer -m 1 1 clock update
  }
}
alias clc  return $chr(3) $+ 7 $+ $1 $+ $chr(3)
on *:close:@clock:.timers off
