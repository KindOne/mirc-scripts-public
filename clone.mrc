; http://hawkee.com/snippet/61/
; Clones snippets
; Tye (tye at mirc . net) DALnet #mIRC / Undernet #mirc.net, #hawkee.com
;
; This collection of snippets does a few things:
;  - Displays all clones in a channel
;  - Displays a user's clones when they join a channel
;  - Kick/Ban clones when they join a channel
;  - Kick/Ban clones in a channel
;
; These scripts are public domain. You can do whatever you want with them.
;
; If you need help with with these snippets please send me a message on mirc.net or email me
; forums.

; To install simply paste the code in your remotes
; The snippets can be used/configured from the channel/nicklist popups
; The 'clone threshold' setting is how many _clones_ are allowed in the channel (not how many users from the same host). A threshold of 0 means no clones are allowed (only 1 user per host)

; /showclones #channel [-k]
; Will display all clones in #channel
; If you give the -k parameter all clones above the channel's threshold will be kicked/banned
alias showclones {
  if ($hget(clones)) hfree clones
  hmake clones 10
  var %x = $nick($1,0)
  while (%x) {
    var %addr = $address($nick($1,%x),2)
    ; Get user's clones
    var %g = $clones(%addr,$1)
    var %n = $gettok(%g,2-,32), %g = $gettok(%g,1,32)
    if (%n) && (!$hget(clones,%addr)) && (!$is_clone_prot($address($nick($1,%x),5))) {
      ; If we haven't already shown clones from this host then do that now
      echo $colour(info) $1 *** %g clones from %addr $+ : %n
      ; If there are more clones than the threshold, and the user is not protected, start kicking
      if ($2 = -k) && (%g > $thresh($chan)) && (!$is_clone_prot($address($nick($1,%x),5))) {
        mode $chan +b %addr
        .timer 1 600 mode $chan -b %addr
        var %xx = $ialchan(%addr,$1,0)
        while (%xx) {
          if ($ialchan(%addr,$1,%xx).nick isreg $1) { kick $chan $ifmatch Clones from %addr }
          dec %xx
        }
      }
      hinc clones %addr
    }
    dec %x
  }
  hfree clones
}

; $clones(*!*@site,#channel)
; Returns the number of clones matching *!*@site and their nicknames in this format:
;    N nicknames
; where N is the number of clones and nicknames is a space delimited list of the users.
alias clones {
  var %x = $ialchan($1,$2,0)
  if (%x = 1) { return 1 }
  if (%x > 80) { return %x Too many to list }
  var %r
  while (%x) {
    %r = %r $ialchan($1,$2,%x).nick
    dec %x
  }
  return $ialchan($1,$2,0) %r
}

; The options below the seperator are per channel settings.
menu channel {
  Clone Utilities
  .Show clones in $chan:showclones #
  .Kick/Ban current clones:if ($input(Are you sure you want to kick/ban all clones in $chan $+ ?,y)) { showclones # -k }
  .$iif($($+(%,clones.join),2),$style(1)) Show clones on join:if ($($+(%,clones.join),2)) { set $+(%,clones.join) 0 } | else { set $+(%,clones.join) 1 }
  .-
  .$iif($($+(%,clones.kick.,$chan),2),$style(1)) Kick/Ban clones on join:if ($($+(%,clones.kick.,$chan),2)) { set $+(%,clones.kick.,$chan) 0 } | else { set $+(%,clones.kick.,$chan) 1 }
  .Clone threshold - $thresh($chan).1:set $+(%,clones.th.,$chan) $input(Enter how many clones a site is allowed in the channel [0 means only 1 user/host $+ $chr(44) 1 means 2 users/host]:,e)
}
menu nicklist {
  $iif(!$is_clone_prot($address($$1,5)),Clone Utilities)
  .Protect User
  ..$submenu($getmasks($1))
  $iif($is_clone_prot($address($$1,5)),Clone Utilities)
  .Unprotect $is_clone_prot($address($$1,5)).addr:ruser $is_clone_prot($address($$1,5)).addr
}
alias -l getmasks {
  if ($1 > 19) { return }
  return $1 $address($snick($active,1),$1) $+ :auser cloneprotect $address($snick($active,1),$1)
}
; $is_clone_prot(nick!user@host)
; Returns $true of user is protected from clone kick
alias is_clone_prot {
  var %x = $ulist(*,cloneprotect,0)
  while (%x) {
    var %a = $ulist(*,cloneprotect,%x)
    if (%a iswm $1) { return $iif($prop = addr,%a,$true) }
    dec %x
  }
  return $false
}
ON *:JOIN:#: {
  ; Show users clones
  var %n = $clones($wildsite,$chan)
  if ($($+(%,clones.join),2)) && (!$is_clone_prot($fulladdress)) {
    if ($gettok(%n,2-,32)) {
      echo -tc info2 $chan * $gettok(%n,1,32) clones from $wildsite $+ : $gettok(%n,2-,32)
    }
  }
  ; If you are opped, there are more clones from the host than the threshold allows, kicking clones has been enabled for this channel and the user is not protected, kick/ban the clones
  if ($me isop $chan) && ($gettok(%n,1,32) > $thresh($chan)) && ($($+(%,clones.kick.,$chan),2)) && (!$is_clone_prot($fulladdress)) {
    ; Ban the host if we haven't already
    if (!$($+(%,clonekicking,$cid,$chan,$wildsite),2)) {
      mode $chan +b $wildsite
      .timer 1 600 mode $chan -b $wildsite
      set -u10 $+(%,clonekicking,$cid,$chan,$wildsite) 1
    }
    var %x = $ialchan($wildsite,$chan,0)
    while (%x) {
      ; Kick this user is we haven't already
      if ($ialchan($wildsite,$chan,%x).nick isreg $chan) && (!$($+(%,clonekicking,$cid,$chan,$ifmatch),2)) {
        var %nick = $ialchan($wildsite,$chan,%x).nick
        kick $chan %nick Clones from $wildsite
        set -u10 $+(%,clonekicking,$cid,$chan,%nick) 1
      }
      dec %x
    }
  }
}
; Returns the clone threshold for a channel
; $thresh(#channel).N
; If you give the N parameter that value will be subtratced from the threshold.
; The threshold returns the maximum number of users a host can have in #channel,
; not the number of clones a host can have. Use $thresh(#channel).1 to get the
; number of clones a host can have.
alias -l thresh {
  if ($gettok($($+(%,clones.th.,$1),2),1,32) != $null) { return $calc($ifmatch + 1 - $prop) }
  return $calc(1 - $prop)
}
