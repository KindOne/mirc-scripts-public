;-- Basic clone scanner
;-- by SplitFire
;-- Usage: /cscan <#channel(Optional)>

alias cscan {
  if (!$1) && ($active !ischan) {
    echo $color(ctcp) -ai2 * /cscan: insufficient parameters
    halt
  }
  var %x 1, %clones, %sets 0, %c $iif($1,$1,#)
  if (%c !ischan) || (%scanchan) halt
  if (!$chan(%c).ial) {
    set -u10 %scanchan %c
    who %c
    halt
  }
  window -c @CloneScan- $+ %c
  while ($nick(%c,%x)) {
    if ($ialchan($address($nick(%c,%x),2),%c,0) > 1) && (!$istok(%clones,$address($nick(%c,%x),2),32)) {
      inc %sets
      var %clones %clones $address($nick(%c,%x),2)
      if (!$window(@CloneScan- $+ %c)) {
        window -da @CloneScan- $+ %c 25 25 500 350
        echo @CloneScan- $+ %c * Clone sets in %c ...
        linesep @CloneScan- $+ %c
      }
      echo @CloneScan- $+ %c * Set (12 $+ %sets $+ ) -04 $address($nick(%c,%x),2) :12 $lclones($address($nick(%c,%x),2),%c)
    }
    inc %x
  }
  if (%sets == 0) echo $color(ctcp) -ai2 * /cscan: No clones found in %c
  else {
    linesep @CloneScan- $+ %c
    echo @CloneScan- $+ %c * Scan complete.
  }
}
alias lclones {
  ; $1 = address / $2 = chan
  var %h 1, %m
  while ($ialchan($1,$2,%h)) {
    %m = %m $ialchan($1,$2,%h).nick
    inc %h
  }
  return $iif(%m,%m,None)
}
raw 315:*: {
  haltdef
  if (%scanchan == $2) {
    unset %scanchan
    cscan $2
  }
}
