; Needs Sat's spaces.dll

alias hex2dec { 
  var %x 0, %y 32, %z 64, %a 96, %b 128, %c 160, %d 192, %e 224 
  var %f +--------------
  echo -ag $+(%f,%f,%f,%f,%f,%f,%f,%f,+)
  while (%x < 32) {

    $dll(spaces.dll, echo, -ag $&
      $chr(124) $iif($len(%x) == 1 , $str($chr(32),2)  0 $+ %x ,$str($chr(32),2) %x ) $str($chr(32),1) $&
      $iif($len($base(%x,10,16)) == 1, 0 $+ $base(%x,10,16),$base(%x,10,16)) $str($chr(32),1) $&
      $chr(124) $iif($len(%y) == 2 ,$str($chr(32),2),$str($chr(32),1)) %y $str($chr(32),1) $&
      $base(%y,10,16) $str($chr(32),1) $&
      $chr(124) $iif($len(%z) == 2 ,$str($chr(32),2),$str($chr(32),1)) %z $str($chr(32),1) $&
      $base(%z,10,16) $str($chr(32),1) $&
      $chr(124) $iif($len(%a) == 2 ,$str($chr(32),2),$str($chr(32),1)) %a $str($chr(32),1) $&
      $base(%a,10,16) $str($chr(32),1) $&
      $chr(124) $str($chr(32),1) %b $str($chr(32),1) $base(%b,10,16) $str($chr(32),1) $&
      $chr(124) $str($chr(32),1) %c $str($chr(32),1) $base(%c,10,16) $str($chr(32),1) $&
      $chr(124) $str($chr(32),1) %d $str($chr(32),1) $base(%d,10,16) $str($chr(32),1) $&
      $chr(124) $str($chr(32),1) %e $str($chr(32),1) $base(%e,10,16) $str($chr(32),1) $chr(124))
    inc %x | inc %y | inc %z | inc %a | inc %b | inc %c | inc %d | inc %e
  }
  echo -ag $+(%f,%f,%f,%f,%f,%f,%f,%f,+)
}
