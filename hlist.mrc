; Usage: /hlist [name/n]
; Displays the Hash Tables currently loaded in mIRC.
; If a table name or number is specified, it will list the items from that table.
; 21-Apr-2010 by Raccoon/EFnet

alias hlist {

  echo -aic info -
  if ($1 == $null) {
    var %i = 1, %n = $hget(0)
    WHILE %i <= %n {
      echo -aic info * %i $+ : $hget(%i) ( $+ $hget(%i,0).item $+ / $+ $hget(%i).size $+ )
      inc %i
    }
  }
  else {
    var %t = $hget($1)
    var %i = 1, %n = $hget(%t,0).item
    WHILE %i <= %n {
      var %item = $hget(%t,%i).item, %data = $hget(%t,%item), %unset = $hget(%t,%item).unset
      echo -aicg info * $base(%i,10,10,3) $+ : %t $+ : $iif(%unset,[[ $+ %unset $+ s]) %item = %data
      inc %i
    }
  }
  echo -aicg info End of /HLIST: %n item(s).
  echo -aicg info -
}
