;options: open on desktop|confirm before removing|highlight|htvedit
on *:load:{
  echo -a Type: /htviewer to load the Hash Table Viewer or /htviewer -u to unload Hash Table Viewer script
  dialog -m htviewer htviewer
}
;0 0 485 335
;0 0 485 500
dialog htviewer {
  title "Hash Table Viewer v1.0 - Created By Slie"
  size 0 0 485 355
  button "Close" 1, 430 310 50 20, ok
  text "Table(s)" 2, 5 10 100 15
  list 3, 5 25 100 240, hsbar
  list 4, 120 25 180 240, hsbar
  list 5, 300 25 180 240, hsbar
  text "Item" 6, 120 10 30 15
  text "Data" 7, 300 10 30 15
  text "" 13, 420 10 48 15
  text "N Slots:" 14, 5 255 100 15

  text "Line #: " 16, 120 255 100 15
  text "use line # for $hget(table/N,#).item " 17, 310 255 200 15

  button "Refresh Tables" 8, 5 310 80 20
  text "Find:" 9, 5 285 50 20
  combo 10, 35 280 100 100, drop
  edit "" 11, 145 280 335 20, autohs
  button "find" 12, 0 0 0 0

  box "Edit" 18, 5 335 475 145
  button "Create Table" 19, 10 350 75 20
  button "Remove Table" 20, 95 350 75 20
  edit "" 21, 180 350 190 20, autohs
  edit "N Slots" 22, 375 350 100 20, autohs

  text "Item: ...." 23, 75 370 50 15
  text "Data: ...." 24, 275 370 50 15

  button "Edit" 25, 10 380 50 20
  edit "" 26, 75 385 200 20, autohs
  edit "" 27, 275 385 200 20, autohs
  text "to:" 28, 50 405 20 15
  edit "" 29, 75 405 200 20, autohs
  edit "" 30, 275 405 200 20, autohs

  button "Add" 31, 10 430 50 20
  edit "" 32, 75 430 200 20, autohs
  edit "" 33, 275 430 200 20, autohs

  button "Delete" 34, 10 455 50 20
  edit "" 35, 75 455 200 20
  edit "" 36, 275 455 200 20

  button "Edit Menu" 37, 145 310 70 20

  button "m" 38, 60 430 15 20
  button "Save Table" 39, 350 310 70 20
  button "Clear" 40, 10 405 35 20

  menu "Options" 41,
  item "Open On Desktop" 42,
  item break, 43
  item "Confirm Before Removing Hash Table" 44
  item break, 45
  item "Highlight Word After Edit/Added" 46
}
on *:dialog:htviewer:init:*:{
  htcheckoptions
  did -a $dname 2 Table(s): $hget(0)
  did -t $dname 12
  did -a $dname 10 Tables | did -a $dname 10 Item | did -a $dname 10 Data
  did -c $dname 10 1
  did -m $dname 26,27
  $gettables($hget(0))

  tokenize 44 %htvoptions
  if ($1 == 1) { did -c $dname 42 }
  if ($2 == 1) { did -c $dname 44 }
  if ($3 == 1) { did -c $dname 46 }
  if ($4 == 0) { dialog -s htviewer 0 0 485 500 }

}
on *:dialog:htviewer:sclick:*:{
  if ($did == 3) { $getnie($hget($httable)) }
  if ($did == 4) {
    did -c $dname 5 $did($dname,4).sel | did -ra $dname 16 Line $chr(35) $+ : $did($dname,4).sel
    $hteditdisplay($dname)
  }
  elseif ($did == 5) {
    did -c $dname 4 $did($dname,5).sel | did -ra $dname 16 Line $chr(35) $+ : $did($dname,5).sel
    $hteditdisplay($dname)
  }
  if ($did == 8) { $gettables($hget(0)) }
  if ($did == 12) { $findht($did($dname,10).seltext,$did($dname,11))  }
  if ($did == 19) {
    if ($hget($did($dname,21))) { var %httemp = $input(Error: Hash Table ( $+ $did($dname,21) $+ ) Already Exist,4) }
    else { $iif($did($dname,21),hmake $did($dname,21) $iif($did($dname,22) isnum,$ifmatch)) | $gettables($hget(0)) }
  }
  if ($did == 20) {
    if (!$hget($did($dname,21))) { var %httemp = $input(Error: Hash table ( $+ $did($dname,21) $+ ) does not exist,4) }
    elseif ($did($dname,21)) {
      if ($gettok(%htvoptions,2,44) == 1) { var %htmp = $input(Are you sure you want to remove the table: $did($dname,21),8)
        if (%htmp) { hfree $did($dname,21) | $gettables($hget(0)) | did -r $dname 4,5,21,26,27,29,30,32,33,35,36 }
      }
      else { hfree $did($dname,21) | $gettables($hget(0)) | did -r $dname 4,5,21,26,27,29,30,32,33,35,36 }
    }
  }
  if ($did == 25) { $htedit($dname) }
  if ($did == 31) { $htadd($dname) }
  if ($did == 34) { $htdelete($dname) }
  if ($did == 37) { htresize }
  if ($did == 38) { if ($gettok(%htvoptions,1,44) = 1) { $iif(!$dialog(htvadd),dialog -dm htvadd htvadd) } | else { $iif(!$dialog(htvadd),dialog -m htvadd htvadd) } }
  if ($did == 39) { htcs }
  if ($did == 40) { did -r $dname 26,27,29,30 }
}
on *:dialog:htviewer:menu:*:{
  if ($did == 42) {
    if ($did($dname,$did).state == 0) { did -c $dname $did | $htsaveoptions(1,1) }
    else { did -u $dname $did | $htsaveoptions(1,0) }
  }
  if ($did == 44) {
    if ($did($dname,$did).state == 0) { did -c $dname $did | $htsaveoptions(2,1) }
    else { did -u $dname $did | $htsaveoptions(2,0) }
  }
  if ($did == 46) {
    if ($did($dname,$did).state == 0) { did -c $dname $did | $htsaveoptions(3,1) }
    else { did -u $dname $did | $htsaveoptions(3,0) }
  }

}
alias -l findht {
  if ((!$1) || (!$2)) { return }
  var %wordfind = $2-, %hcount = 0
  if ($1 == Tables) {
    while (%hcount < $did(htviewer,3).lines) {
      inc %hcount
      if (%wordfind == $gettok($did(htviewer,3,%hcount),2,32)) { did -c htviewer 3 %hcount }
    }
  }
  elseif (($1 == Item) || ($1 == Data)) {
    if ($1 == Item) { var %ht = 4, %ht2 = 5 }
    else { var %ht = 5, %ht2 = 4 }
    while (%hcount < $did(htviewer,%ht).lines) {
      inc %hcount
      if (%wordfind == $did(htviewer,%ht,%hcount)) { did -c htviewer %ht $+ , $+ %ht2 %hcount | did -ra $dname 16 Line $chr(35) $+ : $did($dname,4).sel }
    }

  }
  ;  if ($3 != r) { $hteditdisplay($dname) }
  $hteditdisplay($dname)
}

;name/item/edit
alias -l getnie {
  var %ht = $hget($1,0).item, %hcount = 0, %temp
  did -r htviewer 4,5
  did -ra htviewer 13 ( $+ %ht $+ )
  while (%hcount < %ht) {
    inc %hcount
    %temp = $hget($1,%hcount).item
    did -a htviewer 4 %temp
    did -a htviewer 5 $hget($1,%temp)
  }
  did -ra htviewer 14 N Slots: $hget($1).size
  did -z htviewer 3,4,5

  did -ra htviewer 22 $hget($1).size
  did -ra htviewer 21 $1

  if ($2 != d) { did -ra htviewer 16 Line $chr(35) $+ : }
}
alias -l gettables {
  did -r htviewer 3
  var %hcount = 0
  if ($1 == 0) { did -a htviewer 3 No Tables Exist | return }
  while (%hcount < $1) {
    inc %hcount
    did -a htviewer 3 %hcount $+ : $hget(%hcount)
  }

  did -z htviewer 3,4,5
}
alias htviewer {
  if ($1 == -u) {
    var %check $input(Are you sure you want to unload "Hash Table Viewer" script?,8)
    if (%check) {
      if ($dialog(htviewer)) { dialog -x htviewer }
      unset %htvoptions | unload -rs $script
    }
    else { return }
  }
  var %hpar = -m
  if ($gettok(%htvoptions,1,44) == 1) { %hpar = %hpar $+ d }
  if (!$dialog(htviewer)) { dialog %hpar htviewer htviewer }
  else { echo -a Error: Hast Table Viewer Is Open }
}
alias -l htresize {
  if ($dialog(htviewer).h == 380) { dialog -se htviewer $dialog(htviewer).x $dialog(htviewer).y 485 500 | $htsaveoptions(4,0) }
  else { dialog -se htviewer $dialog(htviewer).x $dialog(htviewer).y 485 355 | $htsaveoptions(4,1) }
}
alias -l htedit {
  var %ht = $1
  var %hte1 = $did(%ht,26), %hte2 = $did(%ht,27), %hte3 = $did(%ht,29), %hte4 = $did(%ht,30)
  if ((!%hte1) && (!%hte2)) { hadd $httable %hte3 %hte4 }
  elseif (%hte1 == %hte3) {
    if (%hte2 == %hte4) { return }
    else { hadd $httable %hte3 %hte4 }
  }
  elseif (%hte1 != %hte3) {
    hdel $httable %hte1
    hadd $httable %hte3 $iif(%hte4,%hte4)
  }

  $getnie($hget($httable))
  if ($gettok(%htvoptions,3,44) == 1) { $findht(item,%hte3) }
  $hteditclear(%ht,%hte3)
}
alias -l htadd {
  var %ht = $1, %htmp = $did($1,32)
  if (!$1) { return }
  if ($did($1,32)) {
    hadd $httable $did($1,32) $iif($did($1,33),$did($1,33))

    $getnie($hget($httable))
  }
  $hteditclear(%ht,$did($1,32))
  if ($gettok(%htvoptions,3,44) == 1) { $findht(item,%htmp) }

  did -r %ht 32,33
}
alias -l htdelete {
  var %ht = $did($1,35)
  if ($hfind($httable,%ht)) { hdel $httable %ht }
  $getnie($hget($httable))
  $hteditclear($1)
}
alias -l httable { return $gettok($did(htviewer,3).seltext,2,58) }
alias -l hteditclear {
  var %ht = $1
  did -r %ht 26,27,29,30,32,33,35,36

  if ($2) { $findht(item,$2,e) }
}
alias -l hteditdisplay {
  var %ht = $1
  did -ra %ht 26,29,35 $did(%ht,4).seltext
  did -ra %ht 27,36 $did(%ht,5,$did(%ht,4).sel).seltext
  did -r %ht 30
}
alias -l htcs {
  var %htcs = $input(Filename to be saved as $+ $chr(44) ie: mytable.hst,1)
  if (%htcs) {
    if (!$exists(%htcs)) { hsave $httable %htcs | echo -a Saved Table: $httable to $mircdir $+ %htcs | return }
    var %htcheck = $input(File exist. Overwrite it?,8)
    if (%htcheck) { .remove %htcs | hsave $httable %htcs | echo -a Saved Table: $httable to $mircdir $+ %htcs | return }
  }
}
alias -l htsaveoptions {
  htcheckoptions
  var %ht = $1, %htvalue = $2, %htcount = 0, %httemp
  while (%htcount < $gettok(%htvoptions,0,44)) {
    inc %htcount
    if (%ht != %htcount) { %httemp = %httemp $+ $gettok(%htvoptions,%htcount,44) $+ $iif(%htcount < 4, $chr(44)) }
    else { %httemp = %httemp $+ %htvalue $+ $iif(%htcount < 4, $chr(44)) }
  }
  %htvoptions = %httemp
}
alias -l htcheckoptions {
  var %ht = 0, %htmp
  while (%ht < 4) {
    inc %ht
    %htmp = $gettok(%htvoptions,1,44)
    if ((%htmp < 0) || (%htmp > 1) || (%htmp !isnum)) { set %htvoptions 0,0,1,0 }
  }
}

dialog htvadd {
  title "Hash Table Viewer: multi add"
  size $dialog(htviewer).x $dialog(htviewer).y 415 310
  button "Add" 1, 85 285 50 20,
  button "Close" 2, 270 285 50 20, cancel
  button "Clear" 31, 180 285 50 20
  text "Item..." 3, 5 5 50 20
  text "Data..." 4, 210 5 50 20
  edit "" 5, 5 20 200 20, autohs
  edit "" 6, 210 20 200 20, autohs
  edit "" 7, 5 40 200 20, autohs
  edit "" 8, 210 40 200 20, autohs
  edit "" 9, 5 60 200 20, autohs
  edit "" 10, 210 60 200 20, autohs
  edit "" 11, 5 80 200 20, autohs
  edit "" 12, 210 80 200 20, autohs
  edit "" 13, 5 100 200 20, autohs
  edit "" 14, 210 100 200 20, autohs
  edit "" 15, 5 120 200 20, autohs
  edit "" 16, 210 120 200 20, autohs
  edit "" 17, 5 140 200 20, autohs
  edit "" 18, 210 140 200 20, autohs
  edit "" 19, 5 160 200 20, autohs
  edit "" 20, 210 160 200 20, autohs
  edit "" 21, 5 180 200 20, autohs
  edit "" 22, 210 180 200 20, autohs
  edit "" 23, 5 200 200 20, autohs
  edit "" 24, 210 200 200 20, autohs
  edit "" 25, 5 220 200 20, autohs
  edit "" 26, 210 220 200 20, autohs
  edit "" 27, 5 240 200 20, autohs
  edit "" 28, 210 240 200 20, autohs
  edit "" 29, 5 260 200 20, autohs
  edit "" 30, 210 260 200 20, autohs
}
on *:dialog:htvadd:init:*:{
  if ($did(htviewer,32)) {
    did -ra $dname 5 $did(htviewer,32)
    did -ra $dname 6 $did(htviewer,33)
  }
}
on *:dialog:htvadd:sclick:1:{
  var %htcount = 4, %ht = $httable
  if (!%ht) { var %ht = $input(Error: No table selected,4) | return }
  while (%htcount < 30) {
    inc %htcount
    if ($did($dname,%htcount)) {
      hadd %ht $did($dname,%htcount) $iif($did($dname,$calc(%htcount + 1)), $ifmatch)
    }
    inc %htcount
  }
  htaddclear
  $getnie($hget($httable))
}
on *:dialog:htvadd:sclick:31:{
  htaddclear
}
alias -l htaddclear {
  var %htmp = 4
  while (%htmp < 30) {
    inc %htmp
    did -r $dname %htmp
  }
}
menu * {
  Hash Table Viewer
  .Open:htviewer
  .-
  .unload:htviewer -u
}
