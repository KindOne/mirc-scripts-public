; KFIND lists users who are in all of the channels you specify.
; It can also list channels that all of the users you specify are in.
; You must also be in all of the same channels for this script to work.
; Usage:  /kfind <#chan #chan ...>  or  /kfind <nick nick ...>
; You can also use this script as an identifier in a remote script.
; Usage:  $kfind(nick nick nick)  or  $kfind(nick,nick,nick)
;
; Example:  /kfind #freenode ##chat ##defocus ##social
; Outputs:  * kfind (#freenode ##chat ##defocus ##social)[3]: ChanServ KindOne Raccoon
; Example:  /kfind KindOne Raccoon
; Outputs:  * kfind (KindOne Raccoon)[3]: ##social ##defocus ##chat
;
; by Raccoon 27-Nov-2014 (v2)
alias kfind {
  ; user specified a list of channels.
  IF ($left($1,1) isin $chantypes) {
    var %chans = $1-
    var %i = 1, %min = 99999, %chan, %minchan
    WHILE ($gettok(%chans,%i,32)) {
      var %chan = $v1
      if ($nick(%chan,0) == $null) { echo -aic notice * kfind: You are not in %chan $+ . | return -1 %chan }
      if ($v1 < %min) { var %minchan = %chan, %min = $v1 }
      inc %i
    }
    var %i = 1, %nchans1 = $calc($numtok(%chans,32) +1), %nick, %nicks
    WHILE ($nick(%minchan,%i)) {
      var %nick = $v1
      var %j = 1
      ; should be optomized.
      WHILE ($gettok(%chans,%j,32)) {
        if (%nick !ison $v1) { break }
        inc %j
      }
      if (%j == %nchans1) { var %nicks = %nicks %nick }
      inc %i
    }
    if (!$isid) { echo -aic notice * kfind ( $+ %chans $+ )[ $+ $numtok(%nicks,32) $+ ]: %nicks }
    return %nicks
  }
  ; user specified a list of nicknames.
  ELSE {
    var %nicks = $1-
    var %i = 1, %min = 99999, %nick, %minnick
    WHILE ($gettok(%nicks,%i,32)) {
      var %nick = $v1
      if ($comchan(%nick,0) == 0) { echo -aic notice * kfind: You do not see %nick $+ . | return -1 %nick }
      if ($v1 < %min) { var %minnick = %nick, %min = $v1 }
      inc %i
    }
    var %i = 1, %nnicks1 = $calc($numtok(%nicks,32) +1), %chan, %chans
    WHILE ($comchan(%minnick,%i)) {
      var %chan = $v1
      var %j = 1
      ; should be optomized.
      WHILE ($gettok(%nicks,%j,32)) {
        if ($v1 !ison %chan) { break }
        inc %j
      }
      if (%j == %nnicks1) { var %chans = %chans %chan }
      inc %i
    }
    if (!$isid) { echo -aic notice * kfind ( $+ %nicks $+ )[ $+ $numtok(%chans,32) $+ ]: %chans }
    return %chans
  }
}