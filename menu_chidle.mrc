MENU Channel,Status {
  -
  Recent Activity
  .$submenu($menu_chidle($1,1))
  My Recent Activity
  .$submenu($menu_chidle($1,2))
}

ALIAS menu_chidle {
  if ($1 == begin) {
    window -lehiz @menu_chidle
    clear @menu_chidle
    var %j = 1
    WHILE $scon(%j) {
      scon %j
      var %k = 1
      WHILE $chan(%k) {
        var %chan = $v1
        if ($2 == 1) var %idle = $chan(%chan).idle
        if ($2 == 2) var %idle = $nick(%chan,$me).idle
        aline @menu_chidle %idle $iif($active == %chan && $activecid == $scon(%j).cid,$style(1)) $&
          %j $+ - $+ $network %chan $+ $chr(9) $+ $dur(%idle,2) $+ : $+ join %chan
        inc %k
      }
      inc %j
    }
    filter -wwctu 1 32 @menu_chidle @menu_chidle
    scon -r
    return
  }

  if ($1 isnum 1-40) {
    return $gettok($line(@menu_chidle,$1),2-,32)
  }

  if ($1 == end) {
    window -c @menu_chidle
    noop
  }
}
; End ALIAS menu_chidle

ALIAS -l dur { return $regsubex($gettok($duration($1),1- $+ $2,32),/(\d+\w)\D*/g,\t) }
 ; by Raccoon
