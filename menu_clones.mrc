; By Raccoon 25-Feb-2011
MENU Nicklist,Query {
  $menu_comchan(init,$1)
  Common Channels ( $+ $comchan($1,0) $+ )
  .$submenu($menu_comchan($1))
}

ALIAS -l menu_comchan {
  ;echo -a $1-
  if $1 == init {
    set -eu1 %menu_comchan.nick $2
  }
  elseif $1 isnum {
    var %nick = %menu_comchan.nick
    var %chan = $comchan(%nick,$1)
    if (%chan) return $remove($nick(%chan,%nick).pnick,%nick) $+ %chan :join %chan
  }
}


MENU Nicklist {
  $iif($0 == 1,$menu_clones(init,$1))
  $iif($0 == 1,Clones)
  .%-mc.a1 ( $+ %-mc.n1 $+ )
  ..$submenu($menu_clones($1,1))
  .%-mc.a2 ( $+ %-mc.n2 $+ )
  ..$submenu($menu_clones($1,2))
  .%-mc.a3 ( $+ %-mc.n3 $+ )
  ..$submenu($menu_clones($1,3))
  .%-mc.a4 ( $+ %-mc.n4 $+ )
  ..$submenu($menu_clones($1,4))
  .$iif(($numtok(%-mc.a5,46) > 2),%-mc.a5 ( $+ %-mc.n5 $+ ))
  ..$submenu($menu_clones($1,5))
  .%-mc.a6 ( $+ %-mc.n6 $+ )
  ..$submenu($menu_clones($1,6))
}

ALIAS -l menu_clones {
  ;echo -a $1-
  if $1 == init {
    set -eu1 %-mc.nick $2
    set -eu1 %-mc.a1 $mask($ial($2),1)
    set -eu1 %-mc.a2 $mask($ial($2),2)
    set -eu1 %-mc.a3 $mask($ial($2),3)
    set -eu1 %-mc.a4 $mask($ial($2),4)
    set -eu1 %-mc.a5 *!*@*. $+ $gettok($deltok(%-mc.a4,1-2,46),-3-,46)
    set -eu1 %-mc.a6 *! $+ $ial($2).user $+ @*
    var %i = 1
    WHILE %i <= 6 {
      var %ii = 1, %s
      WHILE $ialchan($($+(%,-mc.a,%i),2),#,%ii).nick {
        var %s = %s $v1
        if ($len(%s) > 500) break
        inc %ii
      }
      set -eu1 $+(%,-mc.l,%i) $remtok(%s,$2,1,32)
      set -eu1 $+(%,-mc.n,%i) $numtok($($+(%,-mc.l,%i),2),32)
      inc %i
    }
    return
  }
  elseif $1 == begin {
    set -eu1 %-mc.suf 0
  }
  elseif $1 isnum {
    var %mask = $($+(%,-mc.a,$2),2)
    var %nicks = $($+(%,-mc.l,$2),2)
    var %nick = $gettok(%nicks,$1,32)
    if (%nick) return $nick(#,%nick).pnick :sline # %nick
    elseif $1 > 1 {
      inc -eu1 %-mc.suf 1
      if (%-mc.suf == 1) return -
      if (%-mc.suf == 2) return Copy :clipboard %-mc.nick %nicks
      if (%-mc.suf == 3) return Select :mselect # %nicks %-mc.nick
      if (%-mc.suf == 4) return Ban :ban # %mask
      if (%-mc.suf == 5) return Ban/Kick :ban # %mask $(|) mkick # %-mc.nick %nicks
    }
  }
}

ALIAS -l mselect {
  var %i = 2
  WHILE $ [ $+ [ %i ] ] { sline -a $1 $v1 | inc %i }
}

ALIAS -l mkick {
  var %i = 2
  WHILE $ [ $+ [ %i ] ] { kick $1 $v1 No Clones. }
}
