/*
*
*          Permutation Generator
*          ---------------------
*
*  /Permutation <Char-Set>
*
* Example:
*          /Permutations ABC
* Result:
*          ABC
*          ACB
*          BAC
*          BCA
*          CAB
*          CBA
*/
Alias permutations noop $perms($null, $1-)
Alias perms {
  var %i = $len($2)
  ; if (%i == 0) { !echo -a $1 }
  if (%i == 0) { msg #foobar $1 }
  else {
    var %ii = 0
    while (%ii < %i) {
      var %x, %y, %z
      if (%ii) { var %y = $left($2, %ii) }
      scon -r noop $!perms( $1 $+ $mid( $2, $calc(%ii + 1) , 1) ,  %y $+ $mid( $2, $calc(%ii +2) , %i) )
      inc %ii
    }
  }
}
