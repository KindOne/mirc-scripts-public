;---------<  ProjectX - mIRC Scripting Crew  >-----------
; Filename..: pxsc.mrc [ProjectX Spellchecker]
; Release...: v1.00
; Author....: SwitchDragon
; Feedback..: switchdragon@gmail.com
; Date......: Monday, April 10, 2006
; URL.......: No longer active
; Extra.....: Customizable spellchecker with a word filter.
; Short.....: pxsc
; Base......: 3.51
;--------------------------------------------------------

;-------- -  -
; Aliases
;-------- -  -
alias -l dlg dialog $iif($dialog($iif($3,$3,$1)),-v $3,-m $+ $2- $1) $1
alias -l dlgx var %i = $0 | while (%i) { var %d = $ [ $+ [ %i ] ] | if ($dialog(%d)) dialog -x %d | dec %i  }
alias -l pxsc.words { return $+(",$scriptdirpxsc_words.dat,") }
alias -l pxsc.country { return $+(",$scriptdirpxsc_country.dat,") }
alias -l pxsc.filter { return $+(",$scriptdirpxsc_filtered.dat,") }
alias -l pxsc.addsc { hadd pxsctbl $did(5) $did(8) | pxsc.refresh | did -b $dname 9 }
alias -l pxsc.delsc { hdel pxsctbl $hget(pxsctbl,$did(2).sel).item | pxsc.refresh | did -b $dname 9,12 }
alias -l pxsc.addflt { hadd pxsctbl3 $did(2) $did(4) | pxsc.fltrefresh }
alias -l pxsc.delflt { hdel pxsctbl3 $hget(pxsctbl3,$did(15).sel).item | pxsc.fltrefresh | did -b $dname 20 }
alias -l pxsc.refresh {
  var %x = 1
  did -r pxsc 2,5,8
  while ($hget(pxsctbl,%x).item != $null) {
    var %a = $hget(pxsctbl,%x).item
    did -a pxsc 2 %a
    inc %x
  }
}
alias -l pxsc.fltrefresh {
  var %x = 1
  did -r pxsc 15,17
  while ($hget(pxsctbl3,%x).item != $null) {
    var %a = $hget(pxsctbl3,%x).item,%b = $hget(pxsctbl3,%a)
    did -a pxsc 15 %a | did -a pxsc 17 %b
    inc %x
  }
}

;-------- -  -
; Popups
;-------- -  -
menu menubar,status,channel {
  -
  ProjectX Addons
  .$px.nam
  ..Settings: { dlg pxsc }
  ..-
  ..Help: dlg pxsc-help
  ..$iif(!$isfile($px.rm),$style(2)) &Read Me: px.readme
  ..-
  ..&About: px.about
  ..&Unload $px.addon:if ($input(Are you sure you want to unload $+($crlf,$px.nam(1),?),136,ProjectX Unload Confirmation)) .unload -rs $+(",$script,")
  -
}

;-------- -  -
; Dialog
;-------- -  -
dialog pxsc {
  title $px.nam
  size -1 -1 192 136
  option dbu
  tab "Spellchecker", 1, 2 0 186 118
  text "Word list:", 3, 6 18 25 8, tab 1
  box "Add word", 4, 83 38 101 78, tab 1
  edit "", 5, 87 59 92 10, tab 1 autohs
  text "Incorrect word:", 6, 87 51 41 8, tab 1
  text "Correct word:", 7, 88 80 37 8, tab 1
  edit "", 8, 87 88 92 10, tab 1 autohs
  button "Add", 9, 87 102 30 11, tab 1
  check "Enable", 10, 83 25 29 10, tab 1
  button "Delete", 12, 125 102 30 11, tab 1
  list 2, 6 26 70 90, tab 1 size vsbar
  tab "Filter", 13
  text "Word list:", 14, 6 32 26 8, tab 13
  text "Filtered list:", 16, 80 32 30 8, tab 13
  button "Add", 18, 154 41 30 11, tab 13
  button "Delete", 20, 154 55 30 11, tab 13
  list 15, 6 41 67 74, tab 13 size vsbar
  list 17, 80 41 67 74, tab 13 size vsbar
  check "Enable", 19, 7 18 30 10, tab 13
  tab "Options", 22
  check "Capitalize first letter in sentences", 24, 6 18 95 10, tab 22
  check "Capitalize week days", 25, 6 31 78 10, tab 22
  check "Capitalize country names", 26, 6 57 74 10, tab 22
  check "Notify when using all upper case letters", 27, 6 70 105 10, tab 22
  check "Add full stop at the end of sentences", 28, 6 83 99 10, tab 22
  check "Capitalize month names", 35, 6 44 71 10, tab 22
  tab "Statistics", 23
  text "Number of times capitalizing first letter: ", 31, 6 18 95 8, tab 23
  edit "", 32, 113 17 41 10, tab 23 read autohs
  button "Reset", 33, 157 17 29 10, tab 23
  text "Number of times capitalizing country names:", 41, 6 33 106 8, tab 23
  edit "", 42, 113 32 41 10, tab 23 read autohs
  button "Reset", 43, 157 32 29 10, tab 23
  text "Number of times using all upper case letters:", 44, 6 48 107 8, tab 23
  edit "", 45, 113 47 41 10, tab 23 read autohs
  button "Reset", 46, 157 47 29 10, tab 23
  text "Number of times adding full stops:", 47, 6 63 83 8, tab 23
  edit "", 48, 113 62 41 10, tab 23 read autohs
  button "Reset", 49, 157 62 29 10, tab 23
  button "Reset all", 50, 157 105 29 10, tab 23
  text "Number of spelling errors fixed:", 11, 6 78 85 8, tab 23
  edit "", 29, 113 77 41 10, tab 23 read autohs
  text "Number of words filtered:", 30, 6 93 68 8, tab 23
  edit "", 53, 113 92 41 10, tab 23 read autohs
  button "Reset", 54, 157 77 29 10, tab 23
  button "Reset", 55, 157 92 29 10, tab 23
  button "Ok", 51, 44 122 37 12, ok
  button "Cancel", 52, 111 122 37 12, cancel
}

on *:dialog:pxsc:init:*:{
  pxsc.refresh
  pxsc.fltrefresh
  did -b $dname 9,12,20
  did -a $dname 32 %pxsc.cntcapf | did -a $dname 42 %pxsc.cntcapc | did -a $dname 45 %pxsc.cntcap
  did -a $dname 48 %pxsc.cntper | did -a $dname 29 %pxsc.cntwords | did -a $dname 53 %pxsc.cntflt
  if (%pxsc.status == on) { did -c $dname 10 } | else { did -u $dname 10 }
  if (%pxsc.fltstatus == on) { did -c $dname 19 } | else { did -u $dname 19 }
  if (%pxsc.capf == on) { did -c $dname 24 } | else { did -u $dname 24 }
  if (%pxsc.capwd == on) { did -c $dname 25 } | else { did -u $dname 25 }
  if (%pxsc.capm == on) { did -c $dname 35 } | else { did -u $dname 35 }
  if (%pxsc.capc == on) { did -c $dname 26 } | else { did -u $dname 26 }
  if (%pxsc.notcap == on) { did -c $dname 27 } | else { did -u $dname 27 }
  if (%pxsc.per == on) { did -c $dname 28 } | else { did -u $dname 28 }
}

on *:dialog:pxsc:sclick:*:{
  if ($did == 2) { did -r $dname 5,8 | did -e $dname 9,12 | did -a $dname 5 $hget(pxsctbl,$did(2).sel).item | did -a $dname 8 $hget(pxsctbl,$did(5)) }
  if ($did == 9) { pxsc.addsc }
  if ($did == 10) { if (%pxsc.status == off) { set %pxsc.status on } | else { set %pxsc.status off } }
  if ($did == 12) { pxsc.delsc }
  if ($did == 15) { did -e $dname 20 | did -c $dname 17 $did(15).sel }
  if ($did == 17) { did -e $dname 20 | did -c $dname 15 $did(17).sel }
  if ($did == 20) { pxsc.delflt }
  if ($did == 18) { dlg pxsc_add }
  if ($did == 19) { if (%pxsc.fltstatus == off) { set %pxsc.fltstatus on } | else { set %pxsc.fltstatus off } }
  if ($did == 24) { if (%pxsc.capf == off) { set %pxsc.capf on } | else { set %pxsc.capf off } }
  if ($did == 25) { if (%pxsc.capwd == off) { set %pxsc.capwd on } | else { set %pxsc.capwd off } }
  if ($did == 35) { if (%pxsc.capm == off) { set %pxsc.capm on } | else { set %pxsc.capm off } }
  if ($did == 26) { if (%pxsc.capc == off) { set %pxsc.capc on } | else { set %pxsc.capc off } }
  if ($did == 27) { if (%pxsc.notcap == off) { set %pxsc.notcap on } | else { set %pxsc.notcap off } }
  if ($did == 28) { if (%pxsc.per == off) { set %pxsc.per on } | else { set %pxsc.per off } }
  if ($did == 33) { set %pxsc.cntcapf 0 | did -ra $dname 32 %pxsc.cntcapf }
  if ($did == 43) { set %pxsc.cntcapc 0 | did -ra $dname 42 %pxsc.cntcapc }
  if ($did == 46) { set %pxsc.cntcap 0 | did -ra $dname 45 %pxsc.cntcap }
  if ($did == 49) { set %pxsc.cntper 0 | did -ra $dname 48 %pxsc.cntper }
  if ($did == 54) { set %pxsc.cntwords 0 | did -ra $dname 29 %pxsc.cntwords }
  if ($did == 55) { set %pxsc.cntflt 0 | did -ra $dname 53 %pxsc.cntflt }
  if ($did == 50) {
    set %pxsc.cntcapf 0 | set %pxsc.cntcapc 0 | set %pxsc.cntcap 0 | set %pxsc.cntper 0 | set %pxsc.cntwords 0 | set %pxsc.cntflt 0
    did -ra $dname 32 0 | did -ra $dname 42 0 | did -ra $dname 45 0 | did -ra $dname 48 0 | did -ra $dname 29 0 | did -ra $dname 53 0
  }
}

on *:dialog:pxsc:edit:*:{
  if ($did(5) == $null) || ($did(8) == $null) { did -b $dname 9 } | else { did -e $dname 9 }
}

dialog pxsc_add {
  title $px.nam - Add word
  size -1 -1 139 49
  option dbu
  text "Word:", 1, 6 4 18 8
  edit "", 2, 49 3 85 10, autohs
  text "Filtered word:", 3, 6 21 36 8
  edit "", 4, 49 20 85 10, autohs
  button "Ok", 5, 24 35 35 11, disable ok
  button "Cancel", 6, 84 35 35 11, cancel
}
on *:dialog:pxsc_add:edit:*:{ if ($did(2) == $null) || ($did(4) == $null) { did -b $dname 5 } | else { did -e $dname 5 } }
on *:dialog:pxsc_add:sclick:5:{ pxsc.addflt | dialog -x pxsc_add pxsc_add }

dialog pxsc_notcap {
  title $px.nam
  size -1 -1 135 31
  option dbu
  text "The text your attempting to send is all upper case.", 1, 5 3 125 8
  button "Send", 3, 17 18 30 9, ok
  button "Make lower", 4, 51 18 30 10, ok
  button "Cancel", 5, 85 18 30 10, cancel
}

on *:dialog:pxsc_notcap:sclick:*:{
  if ($did == 3) { if (%pxsc.per == on) { msg $active $+(%pxsc.temp,$chr(46)) } | else { msg $active %pxsc.temp } | dialog -x pxsc_notcap pxsc_notcap | unset %pxsc.temp }
  if ($did == 4) { var %temp = $replace(%pxsc.temp,%pxsc.temp,$lower(%pxsc.temp)) | if (%pxsc.per == on) { msg $active $+(%temp,$chr(46)) } | else { msg $active %temp } | dialog -x pxsc_notcap pxsc_notcap | unset %pxsc.temp | halt }
  if ($did == 5) { halt | dialog -x pxsc_notcap pxsc_notcap }
}

dialog pxsc-help {
  title $px.nam - Help
  size -1 -1 200 136
  option dbu
  text "Topics:", 2, 5 4 17 7
  list 3, 5 13 70 102, size
  text "Details:", 4, 80 4 18 7
  edit "", 5, 80 13 115 102, read multi vsbar
  button "&Close", 6, 155 120 40 12, ok
}
on *:dialog:pxsc-help:init:0:{
  if (!$isfile($px.help)) { goto e }
  loadbuf -ottopics $dname 3 $px.help
  did -a $dname 5 Select a topic you wish to find help on.
  if (!$did(3).lines) {
    :e | did -ab $dname 3 Empty!
    did -a $dname 5 No help topics found. | return
  }
}
on *:dialog:pxsc-help:sclick:3:{
  did -r $dname 5
  if (!$readini($px.help,$did(3).seltext,n0)) did -a $dname 5 No help is available for $+(",$did(3).seltext,".)
  elseif (!$readini($px.help,$did(3).seltext,n1)) did -a $dname 5 $readini($px.help,$did(3).seltext,n0)
  else {
    var %n = 0
    while ($readini($px.help,$did(3).seltext,$+(n,%n))) { did -a $dname 5 $ifmatch $+ $iif($readini($px.help,$did(3).seltext,$+(n,$calc(%n + 1))),$+($crlf,$crlf)) | inc %n }
    did -c $dname 5 1
  }
}


;-------- -  -
; Remote
;-------- -  -
on *:INPUT:*:{
  if (%pxsc.status == on) {
    if ($left($1,1) != /) && (!$ctrlenter) {
      var %x = 1,%y = $numtok($1-,32),%temp = $1-
      while (%x <= %y) {
        var %a = $gettok($1-,%x,32)
        if ($hfind(pxsctbl,%a)) { var %b = $hget(pxsctbl,%a),%temp = $replace(%temp,%a,%b) | inc %pxsc.cntwords }
        inc %x
      }
      if (%temp == $null) { var %temp = $1- }
      if (%pxsc.capc == on) {
        var %x = 1,%y = $numtok(%temp,32)
        while (%x <= %y) {
          var %a = $gettok(%temp,%x,32)
          if ($hfind(pxsctbl2,%a)) { var %b = $hget(pxsctbl2,%a),%temp = $replace(%temp,%a,%b) | inc %pxsc.cntcapc }
          inc %x
        }
      }
      if (%pxsc.capf == on) { var %c = $right(%temp,$calc($len(%temp)-1)),%d = $replace($left(%temp,1),a,A,b,B,c,C,d,D,e,E,f,F,g,G,h,H,i,I,j,J,k,K,l,L,m,M,n,N,o,O,p,P,q,Q,r,R,s,S,t,T,u,U,v,V,w,W,x,X,y,Y,z,Z),%temp = $+(%d,%c) | inc %pxsc.cntcapf }
      if (%pxsc.capwd == on) { var %temp = $replace(%temp,sunday,Sunday,monday,Monday,tuesday,Tuesday,wednesday,Wednesday,thursday,Thursday,friday,Friday,saturday,Saturday) }
      if (%pxsc.capm == on) { var %temp = $replace(%temp,january,January,february,February,march,March,april,April,may,May,june,June,july,July,august,August,september,September,october,October,november,November,december,December) }
      if (%pxsc.per == on) { var %a = $right(%temp,1) | if (%a isalnum) { var %temp = $+(%temp,$chr(46)) | inc %pxsc.cntper } }
      if (%pxsc.notcap == on) {
        if (%temp isletter) && (%temp isupper) {
          if ($right(%temp,1) == $chr(46)) {
            var %temp = $left(%temp,$calc($len(%temp)-1))
            dlg pxsc_notcap | set %pxsc.temp %temp | inc %pxsc.cntcap | halt
          }
          else { dlg pxsc_notcap | set %pxsc.temp %temp | inc %pxsc.cntcap | halt }
        }
      }
      msg  $active %temp | halt
    }
  }
}

on ^*:TEXT:*:#:{
  if (%pxsc.fltstatus == on) {
    var %x = 1,%y = $numtok($1-,32),%temp = $1-
    while (%x <= %y) {
      var %a = $gettok($1-,%x,32)
      if ($hfind(pxsctbl3,%a)) { var %b = $hget(pxsctbl3,%a),%temp = $replace(%temp,%a,%b) | inc %pxsc.cntflt }
      inc %x
    }
    if (%temp != $null) { echo -t $chan $+(<,$nick,>) $+($chr(3),$color(normal)) $+ %temp | halt }
  }
}

on ^*:TEXT:*:?:{
  if (%pxsc.fltstatus == on) {
    var %x = 1,%y = $numtok($1-,32),%temp = $1-
    while (%x <= %y) {
      var %a = $gettok($1-,%x,32)
      if ($hfind(pxsctbl3,%a)) { var %b = $hget(pxsctbl3,%a),%temp = $replace(%temp,%a,%b) | inc %pxsc.cntflt }
      inc %x
    }
    if (%temp != $null) { echo -t $nick $+(<,$nick,>) $+($chr(3),$color(normal)) $+ %temp | halt }
  }
}

on *:CONNECT: px.reg
on *:DISCONNECT: unset %px.using
on *:LOAD: {
  if ($version > 6) {
    px.echo -ae $px.nam(1) loaded. (by $px(3) < $+ $px(4) $+ >)
    px.reg | px.about
  }
  hmake pxsctbl 200 | hmake pxsctbl2 100 | hmake pxsctbl3 100
  if ($isfile($pxsc.words)) { hload pxsctbl $pxsc.words } | if ($isfile($pxsc.country)) { hload pxsctbl2 $pxsc.country } | if ($isfile($pxsc.filter)) { hload pxsctbl3 $pxsc.filter }
  set %pxsc.status on | set %pxsc.fltstatus off | set %pxsc.capf off | set %pxsc.capm off | set %pxsc.capwd off | set %pxsc.per off | set %pxsc.capc off | set %pxsc.notcap off
  set %pxsc.cntwords 0 | set %pxsc.cntcapc 0 | set %pxsc.cntcapf 0 | set %pxsc.cntper 0 | set %pxsc.cntcap 0 | set %pxsc.cntflt 0
}
on *:START: {
  if ($version < 6.01) {
    px.echo -ae $px.nam(1) requires mIRC 6.01 or higher. Visit http://www.mirc.com/ for the latest version of mIRC.
    .unload -rs $+(",$script,")
  }
  if ($hget(pxsctbl) == $null) { hmake pxsctbl 200 }
  if ($hget(pxsctbl2) == $null) { hmake pxsctbl2 100 }
  if ($hget(pxsctbl3) == $null) { hmake pxsctbl3 100 }
  if ($isfile($pxsc.words)) { hload pxsctbl $pxsc.words } | if ($isfile($pxsc.country)) { hload pxsctbl2 $pxsc.country } | if ($isfile($pxsc.filter)) { hload pxsctbl3 $pxsc.filter }
}
on *:UNLOAD: {
  if ($hget(pxsctbl) != $null) { hfree pxsctbl }
  if ($hget(pxsctbl2) != $null) { hfree pxsctbl2 }
  if ($hget(pxsctbl3) != $null) { hfree pxsctbl3 }
  unset %pxsc.*
  px.unreg | px.echo -ae $px.nam(1) unloaded successfully. | .unload -rs $+(",$script,")
}
on *:EXIT: { hsave -o pxsctbl $pxsc.words | hsave -o pxsctbl2 $pxsc.country | hsave -o pxsctbl3 $pxsc.filter }

;-------- -  -
; ProjectX
;-------- -  -
alias -l px.about %px.scr = $px(8) | dlg ProjectX
alias -l px.acq return $gettok($read($1,$calc($2 + 1)),3-,32)
alias -l px.nfo var %. = did -a projectx | %. 7 $px.acq($1-,3) | %. 9 $px.acq($1-,4) | %. 11 $remove($px.acq($1-,2),v) - (Base v $+ $px.acq($1-,9) $+ ) | %. 13 $px.acq($1-,5) | %. 15 $px.acq($1-,7)
alias -l px.get return $readini(projectx.ini,$1,$$2)
alias -l px return $gettok($read($+(",$script,"),$calc($1 + 1)),3-,32)
alias -l px.nam return $remove($gettok($px(1),2-,32),[,]) $iif($1,$px(2))
alias -l px.addon return $gettok($px.nam,2-,32)
alias -l px.echo echo $color(info) $1 $px.lp $2-
alias -l px.lp return $iif($isalias(pxt.lp),$pxt.lp,***)
alias -l px.using return ProjectX %px.using (12 $+ $px(6) $+ )
alias -l px.set $iif($3 != $null,writeini,remini) projectx.ini $$1-
alias -l px.www if ($+(",$readini($mircini,files,browser),")) run $ifmatch $$1-
alias -l px.reg if ($server) %px.using = %px.using $+ $iif(%px.using,$chr(44)) $px.addon $+ ( $+ $px(2) $+ )
alias -l px.unreg if ($remtok(%px.using,$matchtok(%px.using,$px.addon,1,44),44)) %px.using = $ifmatch | else unset %px.using
alias -l px.help return $+(",$replace($script,.mrc,-help.cfg),")
alias -l px.rm return $replace($script,mrc,txt)
alias -l px.readme $iif($isfile($px.rm),run $px.rm,px.echo -a Could not locate $nopath($px.rm))
alias -l px.win {
  var %px = drawline -r @px | window -fph +d @px 0 0 276 122 | drawrect -fr @px 0 0 0 0 276 122 | var %i = 0, %x, %y, %m = mIRC Scripting Crew | while (%i < 81) { %x = $calc(147 + %i) | %y = $calc(22 + %i) | %px 4210752 1 %x %y $calc(32 + %x) %y | %px 6316128 1 $calc(1 + %x) $calc(%y - 1) $calc(33 + %x) $calc(%y - 1) | %x = $calc(227 - %i) | %px 4210752 1 %x %y $calc(32 + %x) %y | %px 6316128 1 $calc(1 + %x) $calc(%y - 1) $calc(33 + %x) $calc(%y - 1) | inc %i 4 }
  %px 4210752 1 18 62 193 62 | %px 6316128 1 18 61 193 61 | %px = drawtext -r @px | %px 4210752 arial 27 15 34 Project | %px 4210752 arial 27 16 33 Project | %px 6316128 arial 27 17 32 Project | %i = 0 | %x = 28 | %y = 78 | while (%i < 42) { %px $rgb(%i,%i,%i) arial 14 %x %y %m | dec %y | inc %x | inc %i 3 } | %px 6052956 arial 14 40 65 %m | drawsave @px $+(",$scriptdirpx.bmp,") | window -c @px
}
on *:dialog:projectx:init:0: if (%px.scr == $px(8)) { did -a $dname 2,16,19 $str(�,49) | if (!$isfile($px.rm)) did -b $dname 20 | var %. = $replace($script,mrc,png), %` , %- | if (!$isfile(%.)) px.win | did -g $dname 1 $+(",$iif($isfile(%.),%.,$scriptdirpx.bmp),") | %. = $script(0) | while (%.) { %` = $script(%.) | %- = $nopath(%`) | if ($left(%-,2) == px && $mid(%-,3,-4) != core) { %- = $remove($gettok($px.acq(%`,1),2-,32),[,],ProjectX) | did -i $dname 5 1 %- ( $+ $nopath(%`) $+ ) | if (%` == $script) px.nfo %` } | dec %. } | did -c $dname 5 $didwm(5,$px.addon *,1) }
on *:dialog:projectx:sclick:*: {
  if (%px.scr == $px(8)) {
    var %- = $script($remove($gettok($did(5).seltext,2,40),$chr(41))), %. = $script(0), %px.readme = $replace(%-,mrc,txt) | while (%.) { if ($script(%.) == %-) { %- = $ifmatch | %. = 1 } | dec %. }
    if ($did == 5) { did $iif($isfile(%px.readme),-e,-b) $dname 20 | %. = $replace(%-,mrc,png) | if (!$isfile(%.)) px.win | did -rg $dname 1 $+(",$iif($isfile(%.),%.,$scriptdirpx.bmp),") | px.nfo %- }
    if ($did == 9) px.www $+(mailto:,$did(9),?subject=,$remove($gettok($px.acq(%-,1),2-,32),[,]) $px.acq(%-,2)) | if ($did == 18) px.www $px(6) | if ($did == 20) .run %px.readme | if ($did == 21) unset %px.scr
  }
}
dialog projectx {
  title "ProjectX Addon Information"
  size -1 -1 153 193
  option dbu
  icon 1, 7 7 138 61
  text "", 2, 3 76 147 3,disable
  text " Information ", 3, 6 73 31 7
  text "Addon:", 4, 14 84 19 7
  combo 5, 46 83 100 71,drop
  text "Author:", 6, 14 98 19 7
  text "", 7, 46 98 99 7
  text "E-mail:", 8, 14 108 16 7
  link "", 9, 46 108 99 8
  text "Version:", 10, 14 118 20 7
  text "", 11, 46 118 99 7
  text "Updated:", 12, 14 128 23 7
  text "", 13, 46 128 99 7
  text "Extra Info:", 14, 14 138 27 7
  text "", 15, 46 138 99 14
  text "", 16, 3 155 147 3,disable
  text "The ProjectX Website:", 17, 9 160 54 8
  link $px(6), 18, 67 160 76 7
  text "", 19, 3 171 147 3,disable
  button "&Read Me", 20, 5 177 37 12
  button "&OK", 21, 111 177 37 12,cancel
}
