; r_cycle.mrc * 1999-2012 * Version 2.0 * 06-Jul-2012 * Raccoon/EFnet
; This script has no dependencies. It belongs in the Remotes section.

;###############################################################################
;# This script will automatically cycle (part and rejoin) any channel you are  #
;# in that becomes empty; where you are the last person and do not have ops.   #
;# This can be useful in regaining control of channels when ops has been lost. #
;###############################################################################

On !*:PART:#: CYCLE_IF_EMPTY $chan $nick
On !*:KICK:#: if ($knick != $me) CYCLE_IF_EMPTY $chan $nick
On !*:QUIT: var %i = 1 | WHILE $comchan($nick,%i) { CYCLE_IF_EMPTY $v1 $nick | inc %i }

ALIAS -l CYCLE_IF_EMPTY {
  var %chan = $1, %nick = $2
  if $nick(%chan,0) != 2            { return } | ;You are alone.
  ;if $me isop %chan                { return } | ;You are not opped.
  if $nick(%chan,$me,@&~!)          { return } | ;You are not op/owner/founder.
  if r isincs $chan(%chan).mode     { return } | ;Channel is unregistered.
  if %nick == ChanServ              { return } | ;Don't battle with ChanServ
  if $($+(%,CYCLED.,$cid,%chan),2)  { return } | ;Flood prevention.
  set -eu10 $+(%,CYCLED.,$cid,%chan) $true     | ;Remember we cycled for 10 sec.
  ;-----
  echo 4 -ti %chan * Cycling %chan ...
  flash %chan                                  | ;Alert with flashing.
  window -g2 %chan                             | ;Alert with highlighting.
  !hop -cn %chan Cycling!                      | ;Part and rejoin channel.
}

; End of /NAMES list. You've rejoined the channel.
RAW 366:*: {
  var %chan = $2
  if $($+(%,CYCLED.,$cid,%chan),2)  { return } | ;You recently cycled this chan.
  ;if $me !isop %chan               { return } | ;You are now opped!
  if !$nick(%chan,$me,@&~!)         { return } | ;You are now op/owner/founder!
  ;-----
  ;mode %chan +ntpsi
  ;mode %chan +eI $mask($ial($me),3) $mask($ial($me),3)
  ;topic %chan This channel is mine now.
  ;invite MyBot %chan

}

; End of script.

; === Change Log ===
;
; v2.0 -- 06-Jul-2012
; * Greatly consolidated script and comments. "Too wordy".
; * Changed 'isop' to '$nick($chan,$me,@&~!)' for other greater-than op modes.
; * Fixed buggy syntax with flood prevention.
;
; v1.1 -- 22-Feb-2010
; * Consolidated cycle conditions to alias CYCLE_IF_EMPTY
; * Added conditions to check for presence of ChanServ or mode +r (registered chan)
; * Added $cid to %CYCLED.$chan variable for multi-server suppport.
; * To prevent flooding, a channel wont be cycled more than once in 10 seconds.
; * Changed call from 'hop' command to !hop to bypass old /hop aliases
;
; v1.0 -- 21-Feb-2010
; * First public release of script for mIRC v6.35
; * Rewritten from scratch, based on a similar script written in 1999.
;
; v0.0 -- before 09-Apr-1999
; * My first cycle script (cycle.ini) for mIRC v5.51
; * Included features such as playing back chat log, restore colors,
; * etc., features we take for granted now with the /hop command.
;
; ==================
