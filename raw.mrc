/*
* Used to manipulate the output of various commands.
*
*
*
*/



raw 0:*:{
  if (!$2) {
    if ($dialog(silence)) {
      if (+*!*@* iswm $1) {
        did -a silence 1 $mid($1,2)
        did -e silence 1,2,3,4
      }
      elseif (-*!*@* iswm $1) {
        while ($didwm(silence,1,$mid($1,2),1)) { did -d silence 1 $v1 }
        did -e silence 1,2,3,4
      }
      haltdef
    }
  }
}

raw 001:*:{
  set %rconnected. $+ $cid 1
  .signal -n connect
}
raw 005:*:{
  if ($gettok($wildtok($1-,TOPICLEN=?*,1,32),2,61)) { set %topiclen. $+ $cid $v1 }
  if ($gettok($wildtok($1-,SILENCE=?*,1,32),2,61)) { set %silencenum. $+ $cid $v1 }
  if ($gettok($wildtok($1-,MAXBANS=?*,1,32),2,61)) { set %maxbans. $+ $cid $v1 }
}
raw 004:*:{ set %usermodes. $+ $cid $4 }



; This OO5 is copied from http://forum.swiftirc.net/viewtopic.php?f=34&t=17837
/*


raw 005:*: {
  ; if ($hget(005 $+ $network)) return
  var %x 1
  tokenize 32 $2-
  var %i = 1
  while $ [ $+ [ %i ] ] != are {
    hadd -m raw- $+ $network $token($1,1,61) $iif($token($1,2,61) != $null,$v1,$true)
    tokenize 32 $2-
    inc %1
  }
}
alias raw005 {
  if ($hget(raw- $+ $1)) {
    if (!$prop) return $true
    if ($hget(raw- $+ $1,$prop) != $null) return $v1
  }
  return $false
}
on *:DISCONNECT: {
  var %x 1
  while ($scon(%x)) {
    if ($v1 != $cid && $scon(%x).network = $network) return
    inc %x
  }
  hfree raw- $+ $network
}


*/



; /stats p
raw 249:*:{
  haltdef
  if ($3 !isnum) {
    ; Plexus...
    if ($7) {
      echo -as $2 $3 $4 $5 $6 $7 - $duration($7,n)
    }
    ; Charybdis does not add the idle time
    else {
      echo -as $2 $3 $4 $5 $6
    }
  }
}


raw 307:*:{
  haltdef
  echo -ag Identified: $2
}



raw 310:*:{
  haltdef
  echo -ag Modes: $6-
}
raw 311:*:{
  haltdef
  echo -ag Nick: $2
  echo -ag Ident: $3
  echo -ag Host: $4
  echo -ag Name: $6-
}


; Whowas -- violation of RFC 1459?
raw 312:*:{
  haltdef
  echo -ag Total Channels: %total_channels
  unset %total_channels
  echo -ag Server: $3-
  ; Must check for a $ctime, since all the IRCd's break this RFC...
  if ($ctime($4-)) {
    echo -ag Sign OFF: $4-
    echo -ag  Signed off ago: $duration($calc($ctime - $ctime($4-)))
  }
}


raw 313:*:{
  haltdef
  echo -ag $2-
}


; Whowas
raw 314:*:{
  haltdef
  echo -ag Nick: $2
  echo -ag Ident: $3
  echo -ag Host: $4
  echo -ag Name: $6-

}


raw 317:*:{
  haltdef
  ; :irc.foobar.com 317 KindOne KindOne 70403 1437062834 :seconds idle, signon time
  echo -ag Idle: $duration($3) - $asctime($calc($ctime - $3),dddd dd mmm yyyy HH:nn:ss zzz)
  echo -ag Connected: $duration($calc($ctime - $4)) - $asctime($4,dddd dd mmm yyyy HH:nn:ss zzz)
  ; This was between the two...
  ; echo -ag Sign On: $asctime($4,dddd dd mmm yyyy HH:nn:ss zzz)
  ; echo -ag Connected: $duration($calc($ctime - $4))
}

raw 318:*:{
  haltdef
  echo -ag   $+ $2 $+  $3-
}
raw 319:*:{
  haltdef
  echo -ag Channels: $3-
  inc %total_channels $calc($0 -2)
}


raw 323:*:{
  if ($network == freenode) {
    save
  }
}

raw 330:*:{
  haltdef
  echo -ag Identified: $3
}
raw 331:*:{

}
; Topic
raw 332:*:{ haltdef }
; Topic Set by/time
raw 333:*:{ haltdef }


raw 338:*:{
  haltdef
  echo -ag Host: $3
}


; ------------------ /mode #channel e

raw 348:*:{
  haltdef
  echo -s $2 $3 set by $4  $duration($calc($ctime - $5)) - ( $+ $asctime($5) $+ )
}
; End of Exception List. No Need to modify


raw 352:*:{ hadd -m servers $5 }


; Grab the current number of people in the channel when we join.
raw 366:*:{
  if ($nick($2,0)) && ((!$hget(peaks,$network $+ $2)) || ($wd($hget(peaks,$network $+ $2),1) < $nick($2,0))) {
    hadd -m peaks $network $+ $2 $nick($2,0) $ctime
  }
}


; ------------------ /mode #channel b

raw 367:*:{
  haltdef
  ; :irc.freenode.net 367 KindOne #happy *!*@*communism* KindOne!KindOne@KindOne. 1335409528
  echo -s $asctime([HH:nn:ss]) $2 $3 set by $4  $duration($calc($ctime - $5)) - ( $+ $asctime($5) $+ )
}
; End of Ban List. No Need to modify

raw 368:*:{ }


; Whowas
raw 369:*:{
  haltdef
  echo -ag   $+ $2 $+  $3-
}


raw 378:*:{
  haltdef
  echo -ag IP: $6
}

; Erroneous Nickname
raw 432:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2-
}
; Nickname is already in use.
raw 433:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2-
}

raw 671:*:{
  haltdef
  echo -ag Connection: SSL
}


; ------------------ /mode KindOne +g

; This add's timestamp to the 3 RAW numerics ralated to usermode +g
; Could use $timestamp, but $asctime is more customisable.

; [18:38:11] EvilOne is in +g mode (server-side ignore.)
raw 716:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2-
}


; [18:38:11] EvilOne has been informed that you messaged them.
raw 717:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2-
}


; [18:38:11] EvilOne ident@host is messaging you, and you have umode +g.
raw 718:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2-
}



; ------------------ /mode #channel q
; Apparently charybdis decided to move the numerics around...
; Quiet lists.
raw 728:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2 $4 set by $5 $duration($calc($ctime - $6)) - ( $+ $asctime($6) $+ )
}

; The end of the quiet list has been butchered, fix it
raw 729:*:{
  haltdef
  echo -s $asctime([HH:nn:ss]) $2 $4-
}
