; $rot13 is an alias to implement the ROT13 algorithm for encoding text.
; If you don't know what ROT13 encoding is, take a look at
; http://dictionary.reference.com/search?q=rot13
;
; $rot13 is used both to encipher and decipher the text. This is NOT
; meant to be used as encryption. To use this alias simply do,
; $rot13(the text):
;
; $rot13(this is just a test) = guvf vf whfg n grfg
; $rot13(guvf vf whfg n grfg) = this is just a test
;
; (c) 2003 codemastr (Dominick Meglio) [codemastr@unrealircd.com]
; This file is free to use in any script so long as the above copyright message remains intact.
; If you need any help with this you can usually find me on irc.axenet.org in #mircscripting

alias rot13 {
  var %r, %i = $len($1-)
  while (%i > 0) {
    var %t = $mid($1-,%i,1)
    if (%t !isalpha) %r = %t $+ %r
    else if ($isupper(%t)) %r = $chr($calc(($asc(%t) - 52) % 26 + 65)) $+ %r
    else %r = $chr($calc(($asc(%t) - 84) % 26 + 97)) $+ %r
    dec %i
  }
  return %r
}
