; Copied from 'Wims' irc.swiftirc.net #msl
; Bugs - Does not like text with a space aka $chr(32) in it...

alias msort {
  fopen -no sort sword.txt
  filter -fk word.txt cmsort *
  fclose sort
}

alias cmsort {
  if (%cmsort == $null) %cmsort = $1- $lf
  elseif ($sorttok(%cmsort $+ $1-,10)) {
    .fwrite -n sort $replace($v1,$chr(10),$crlf)
    unset %cmsort
  }
}
