; Copied from 'Wims' - irc.swiftirc.net #msl

dialog vtable {
  title "vtable"
  size -1 -1 294 77
  option dbu
  list 1, 3 9 42 66, size hsbar vsbar sort
  list 2, 48 9 42 66, size hsbar vsbar sort
  edit "", 3, 93 9 198 10, autohs
  box "Table", 4, 1 1 46 75
  box "Data", 5, 91 1 202 75
  button "Ok", 6, 170 41 37 12, ok
  box "Item", 7, 46 1 46 75
}

alias vtable dialog -dmo vtable vtable

; Changed so it can load a bazillion more items.

;on *:dialog:vtable:init:*:didtok vtable 1 32 $regsubex($str(a,$hget(0)),/(.)/g,$+($hget(\n),$chr(32))) | did -z vtable 1
;on *:dialog:vtable:sclick:1:if ($did(1).seltext != $null) { did -r vtable 2 | didtok vtable 2 32 $regsubex($str(a,$hget($v1,0).item),/(.)/g,$+($hget($v1,\n).item,$chr(32))) | did -z vtable 2 | did -r vtable 3 }


on *:dialog:vtable:init:*:var %a 1 | while ($hget(%a)) { did -a vtable 1 $v1 | inc %a } | did -z vtable 1
on *:dialog:vtable:sclick:1:if ($did(1).seltext != $null) { did -r vtable 2 | var %a 1,%v $v1 | while ($hget(%v,%a).item) { did -a vtable 2 $v1 | inc %a } | did -z vtable 2 | did -r vtable 3 }


on *:dialog:vtable:sclick:2:if ($did(2).seltext != $null) did -ra vtable 3 $hget($did(1).seltext,$v1)
on *:dialog:vtable:sclick:6:if ($hget($$did(1).seltext)) hadd $v1 $$did(2).seltext $did(3)
on *:dialog:vtable:edit:3:if ($hget($$did(1).seltext)) hadd $v1 $$did(2).seltext $did(3)
