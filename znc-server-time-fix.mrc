; ZNC 1.7.0 added support for server-time.
; mIRC 7.33 - 7.41 will stall/hang during the connection.
; Remove support for server-time and finish the CAP so
; mirc can connect.
raw *:*:{
  if (($version < 7.33) || ($version > 7.41)) { return }
  if (($1 == irc.znc.in) && ($4-5 == ACK :server-time)) {
    cap req -server-time
    cap end
  }
}
